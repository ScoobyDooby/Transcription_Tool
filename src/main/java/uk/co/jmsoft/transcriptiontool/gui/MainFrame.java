/*
 * The MIT License
 *
 * Copyright 2017 John Marshall.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uk.co.jmsoft.transcriptiontool.gui;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLayer;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.plaf.LayerUI;
import uk.co.jmsoft.transcriptiontool.exceptions.AudioFileFailedToLoadException;
import uk.co.jmsoft.transcriptiontool.exceptions.BufferBoundsException;
import uk.co.jmsoft.transcriptiontool.core.Model;
import uk.co.jmsoft.transcriptiontool.exceptions.LoopBoundsException;
import uk.co.jmsoft.transcriptiontool.exceptions.LooperNotInitialisedException;
import uk.co.jmsoft.transcriptiontool.exceptions.NoAudioFileLoadedException;
import uk.co.jmsoft.transcriptiontool.exceptions.PlayModeException;
import uk.co.jmsoft.transcriptiontool.core.TrackEndListener;
import uk.co.jmsoft.transcriptiontool.exceptions.SpeedRatioException;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.InputVerifier;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import uk.co.jmsoft.savehandler.OpenRecentFileListener;
import uk.co.jmsoft.savehandler.RecentFilesUpdateListener;
import uk.co.jmsoft.savehandler.Result;
import uk.co.jmsoft.savehandler.SwingSaveHandler;
import uk.co.jmsoft.savehandler.exceptions.SaveStateOperationFailedException;

/**
 *
 * @author Scooby
 */
public class MainFrame extends javax.swing.JFrame
{

    private class Preference
    {

        private static final String filePath = "filePath";
    }

    private final String preferencesNodeName = "Transcription Tool";
    private Model core;
    private boolean playing = false;
    private final Preferences preferences;
    private Timer timer;
    private final SaveFileFilter saveFileFilter = new SaveFileFilter();
    private final AudioFileFilter audioFileFilter = new AudioFileFilter();
    private final SwingSaveHandler saveHandler;

    /**
     * Creates new form Main
     */
    public MainFrame()
    {
        initComponents();
        setTitle("Transcription Tool");
        // initialise core
        core = new Model();
        core.addTrackEndListener(trackEndListener);

        saveHandler = new SwingSaveHandler(core, this, this, preferencesNodeName);
        saveHandler.setFileFilter(saveFileFilter);
        saveHandler.setHandleRecentFiles(true);
        saveHandler.addRecentFilesUpdateListener(recentFilesUpdateListener);
        saveHandler.addOpenRecentFileListener(openRecentFileListener);
        
        LayerUI<JComponent> transportLayerUI = new TransportBarLayerUI();
        JLayer<JComponent> panel1JLayer = new JLayer<>(progressBarJPanel1, transportLayerUI);
        transportBarPanel.add(panel1JLayer);

        SpinnerModel speedRatioSpinnerModel = new SpinnerNumberModel(1.0, 0.1, 2.0, 0.01);
        speedRatioSpinner.setModel(speedRatioSpinnerModel);

        // limit resizing currently this approach messes up the maximise/restore down button
//        addComponentListener(new java.awt.event.ComponentAdapter()
//        {
//            int fixedHeight = 370;
//            @Override
//            public void componentResized(ComponentEvent event)
//            {
//                setSize(getWidth(), fixedHeight);
//            }
//        });
        this.pack();

        loopListComboBoxExtra.addActionListener(loopListComboBoxListener);

        // timer for gui updates during play.
        timer = new Timer(100, timerListener);

        // preferences
        preferences = Preferences.userRoot().node(preferencesNodeName);
        String filePath = preferences.get(Preference.filePath, System.getProperty("user.home"));
        saveHandler.setCurrentDirectory(new File(filePath));

        updateRecentFilesMenu();

        // update gui
        setGUIToNoSaveLoaded();
    }

    private void enableRecentFileMenu()
    {
        recentMenu.setEnabled(recentMenu.getPopupMenu().getComponentCount() > 0);
    }

    TrackEndListener trackEndListener = new TrackEndListener()
    {

        @Override
        public void onTrackEnd()
        {
            playing = false;
            setGUINotPlaying();
        }
    };

    ActionListener timerListener = new ActionListener()
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            long currentSample = core.getCurrentFrameNumber();
            currentTimeLabel.setText(convertSamplesToMinSecSamp(currentSample));
            progressBarJPanel1.setCurrentPosition((int) currentSample);
        }
    };

    ActionListener loopListComboBoxListener = new ActionListener()
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            try
            {
                core.setCurrentLoop((String) loopListComboBoxExtra.getSelectedItem());
            }
            catch (PlayModeException ex)
            {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "Tried to change loop while playing loop", ex);
                System.exit(0);
            }
            if (loopListComboBoxExtra.getSelectedItem() != null)
            {
                updateLoopBounds();
                try
                {
                    updateTransport();
                }
                catch (NoAudioFileLoadedException ex)
                {
                    //Should not be possible
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "No Audio file loaded when updating transport after selected loop changed.", ex);
                    System.exit(0);
                }
            }
        }
    };

    /**
     * This method is called from within the constructor to
     * initialise the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        fileChooser = new javax.swing.JFileChooser();
        progressBarJPanel1 = new uk.co.jmsoft.progressbarjpanel.ProgressBarJPanel();
        mainTransportPanel = new javax.swing.JPanel();
        currentTimeLabel = new javax.swing.JLabel();
        trackToStartButton = new javax.swing.JButton();
        playTrackButton = new javax.swing.JButton();
        totalTimeLabel = new javax.swing.JLabel();
        stopTrackButton = new javax.swing.JButton();
        transportBarPanel = new javax.swing.JPanel();
        speedRatioSpinner = new javax.swing.JSpinner();
        speedRatioLabel = new javax.swing.JLabel();
        setSpeedButton = new javax.swing.JButton();
        loopPanel = new javax.swing.JPanel();
        leftLoopPanel = new javax.swing.JPanel();
        newLoopButton = new javax.swing.JButton();
        loopListComboBoxExtra = new uk.co.jmsoft.comboboxextra.ComboBoxExtra();
        duplicateLoopButton = new javax.swing.JButton();
        deleteLoopButton = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        centerLoopPanel = new javax.swing.JPanel();
        previousLoopButton = new javax.swing.JButton();
        nextLoopButton = new javax.swing.JButton();
        setLoopEndButton = new javax.swing.JButton();
        setLoopStartButton = new javax.swing.JButton();
        loopStartSpinner = new javax.swing.JSpinner();
        loopEndSpinner = new javax.swing.JSpinner();
        rightLoopPanel = new javax.swing.JPanel();
        playLoopedButton = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        newSaveMenuItem = new javax.swing.JMenuItem();
        openMenuItem = new javax.swing.JMenuItem();
        recentMenu = new javax.swing.JMenu();
        saveMenuItem = new javax.swing.JMenuItem();
        saveAsMenuItem = new javax.swing.JMenuItem();
        exitMenuItem = new javax.swing.JMenuItem();
        trackMenu = new javax.swing.JMenu();
        playTrackMenuItem = new javax.swing.JMenuItem();
        stopTrackMenuItem = new javax.swing.JMenuItem();
        trackToStartMenuItem = new javax.swing.JMenuItem();
        loopMenu = new javax.swing.JMenu();
        playLoopMenuItem = new javax.swing.JMenuItem();
        stopLoopMenuItem = new javax.swing.JMenuItem();
        loopToStartMenuItem = new javax.swing.JMenuItem();
        previousLoopMenuItem = new javax.swing.JMenuItem();
        nextLoopMenuItem = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        newLoopMenuItem = new javax.swing.JMenuItem();
        duplicateMenuItem = new javax.swing.JMenuItem();
        renameLoopMenuItem = new javax.swing.JMenuItem();
        deleteMenuItem = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        progressBarJPanel1.setBackground(new java.awt.Color(226, 189, 255));
        progressBarJPanel1.setForeground(new java.awt.Color(255, 157, 19));

        javax.swing.GroupLayout progressBarJPanel1Layout = new javax.swing.GroupLayout(progressBarJPanel1);
        progressBarJPanel1.setLayout(progressBarJPanel1Layout);
        progressBarJPanel1Layout.setHorizontalGroup(
            progressBarJPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        progressBarJPanel1Layout.setVerticalGroup(
            progressBarJPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximizedBounds(new java.awt.Rectangle(0, 0, 2147483647, 380));
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });

        mainTransportPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(142, 142, 142)), "Track"));

        currentTimeLabel.setText("Current time");

        trackToStartButton.setAction(trackToStartAction);
        trackToStartButton.setText("To start");

        playTrackButton.setAction(playTrackAction);
        playTrackButton.setText("Play");

        totalTimeLabel.setText("Total time");

        stopTrackButton.setAction(stopTrackAction);
        stopTrackButton.setText("Stop");

        transportBarPanel.setBackground(new java.awt.Color(255, 255, 255));
        transportBarPanel.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        transportBarPanel.setLayout(new javax.swing.BoxLayout(transportBarPanel, javax.swing.BoxLayout.LINE_AXIS));

        speedRatioLabel.setText("Speed ratio");

        setSpeedButton.setText("Set");
        setSpeedButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                setSpeedButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout mainTransportPanelLayout = new javax.swing.GroupLayout(mainTransportPanel);
        mainTransportPanel.setLayout(mainTransportPanelLayout);
        mainTransportPanelLayout.setHorizontalGroup(
            mainTransportPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainTransportPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainTransportPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(transportBarPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(mainTransportPanelLayout.createSequentialGroup()
                        .addComponent(currentTimeLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(totalTimeLabel))
                    .addGroup(mainTransportPanelLayout.createSequentialGroup()
                        .addComponent(playTrackButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(stopTrackButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(trackToStartButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(speedRatioLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(speedRatioSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(setSpeedButton)))
                .addContainerGap())
        );

        mainTransportPanelLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {playTrackButton, stopTrackButton, trackToStartButton});

        mainTransportPanelLayout.setVerticalGroup(
            mainTransportPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainTransportPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(transportBarPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mainTransportPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totalTimeLabel)
                    .addComponent(currentTimeLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mainTransportPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(playTrackButton)
                    .addComponent(stopTrackButton)
                    .addComponent(trackToStartButton)
                    .addComponent(speedRatioSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(speedRatioLabel)
                    .addComponent(setSpeedButton))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        loopPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(142, 142, 142), 1, true), "Loops"));

        leftLoopPanel.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(142, 142, 142), 1, true));

        newLoopButton.setAction(newLoopAction);
        newLoopButton.setText("New");

        duplicateLoopButton.setAction(duplicateLoopAction);
        duplicateLoopButton.setText("Duplicate");

        deleteLoopButton.setAction(deleteLoopAction);
        deleteLoopButton.setText("Delete");

        jButton1.setAction(renameLoopAction);
        jButton1.setText("Rename");

        javax.swing.GroupLayout leftLoopPanelLayout = new javax.swing.GroupLayout(leftLoopPanel);
        leftLoopPanel.setLayout(leftLoopPanelLayout);
        leftLoopPanelLayout.setHorizontalGroup(
            leftLoopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(leftLoopPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(leftLoopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(leftLoopPanelLayout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(deleteLoopButton))
                    .addGroup(leftLoopPanelLayout.createSequentialGroup()
                        .addComponent(loopListComboBoxExtra, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(leftLoopPanelLayout.createSequentialGroup()
                        .addComponent(newLoopButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(duplicateLoopButton)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        leftLoopPanelLayout.setVerticalGroup(
            leftLoopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(leftLoopPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(loopListComboBoxExtra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(leftLoopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(newLoopButton)
                    .addComponent(duplicateLoopButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(leftLoopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(deleteLoopButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        centerLoopPanel.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(142, 142, 142), 1, true));

        previousLoopButton.setAction(previousLoopAction);
        previousLoopButton.setText("Previous");

        nextLoopButton.setAction(nextLoopAction);
        nextLoopButton.setText("Next");

        setLoopEndButton.setAction(setEndAction);
        setLoopEndButton.setText("Set End");

        setLoopStartButton.setAction(setStartAction);
        setLoopStartButton.setText("Set Start");

        loopStartSpinner.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                loopStartSpinnerStateChanged(evt);
            }
        });

        loopEndSpinner.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                loopEndSpinnerStateChanged(evt);
            }
        });

        javax.swing.GroupLayout centerLoopPanelLayout = new javax.swing.GroupLayout(centerLoopPanel);
        centerLoopPanel.setLayout(centerLoopPanelLayout);
        centerLoopPanelLayout.setHorizontalGroup(
            centerLoopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(centerLoopPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(centerLoopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(centerLoopPanelLayout.createSequentialGroup()
                        .addComponent(previousLoopButton)
                        .addGap(23, 23, 23))
                    .addGroup(centerLoopPanelLayout.createSequentialGroup()
                        .addGroup(centerLoopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(loopStartSpinner)
                            .addGroup(centerLoopPanelLayout.createSequentialGroup()
                                .addComponent(setLoopStartButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(20, 20, 20)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(centerLoopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nextLoopButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(loopEndSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(setLoopEndButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        centerLoopPanelLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {nextLoopButton, previousLoopButton, setLoopEndButton, setLoopStartButton});

        centerLoopPanelLayout.setVerticalGroup(
            centerLoopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(centerLoopPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(centerLoopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(previousLoopButton)
                    .addComponent(nextLoopButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(centerLoopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(loopStartSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(loopEndSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(centerLoopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(setLoopStartButton)
                    .addComponent(setLoopEndButton))
                .addContainerGap())
        );

        rightLoopPanel.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(142, 142, 142), 1, true));

        playLoopedButton.setAction(playLoopedAction);
        playLoopedButton.setText("Loop");

        jButton2.setAction(stopLoopAction);
        jButton2.setText("Stop");

        jButton3.setAction(loopToStartAction);
        jButton3.setText("To start");

        javax.swing.GroupLayout rightLoopPanelLayout = new javax.swing.GroupLayout(rightLoopPanel);
        rightLoopPanel.setLayout(rightLoopPanelLayout);
        rightLoopPanelLayout.setHorizontalGroup(
            rightLoopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rightLoopPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(rightLoopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(playLoopedButton)
                    .addComponent(jButton3)
                    .addComponent(jButton2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        rightLoopPanelLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jButton2, jButton3, playLoopedButton});

        rightLoopPanelLayout.setVerticalGroup(
            rightLoopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rightLoopPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(playLoopedButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton3)
                .addContainerGap())
        );

        javax.swing.GroupLayout loopPanelLayout = new javax.swing.GroupLayout(loopPanel);
        loopPanel.setLayout(loopPanelLayout);
        loopPanelLayout.setHorizontalGroup(
            loopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(loopPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(leftLoopPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(centerLoopPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rightLoopPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8))
        );
        loopPanelLayout.setVerticalGroup(
            loopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(loopPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(loopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(leftLoopPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(loopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(centerLoopPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(rightLoopPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        fileMenu.setMnemonic('F');
        fileMenu.setText("File");

        newSaveMenuItem.setAction(loadAudioFileAction);
        newSaveMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        newSaveMenuItem.setMnemonic('n');
        newSaveMenuItem.setText("New...");
        fileMenu.add(newSaveMenuItem);

        openMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        openMenuItem.setMnemonic('O');
        openMenuItem.setText("Open...");
        openMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                openMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(openMenuItem);

        recentMenu.setMnemonic('R');
        recentMenu.setText("Recent");
        fileMenu.add(recentMenu);

        saveMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        saveMenuItem.setMnemonic('s');
        saveMenuItem.setText("Save");
        saveMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                saveMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveMenuItem);

        saveAsMenuItem.setText("Save As...");
        saveAsMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                saveAsMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveAsMenuItem);

        exitMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.ALT_MASK));
        exitMenuItem.setMnemonic('x');
        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        trackMenu.setMnemonic('T');
        trackMenu.setText("Track");

        playTrackMenuItem.setAction(playTrackAction);
        playTrackMenuItem.setText("Play");
        trackMenu.add(playTrackMenuItem);

        stopTrackMenuItem.setAction(stopTrackAction);
        stopTrackMenuItem.setText("Stop");
        trackMenu.add(stopTrackMenuItem);

        trackToStartMenuItem.setAction(trackToStartAction);
        trackToStartMenuItem.setText("To start");
        trackMenu.add(trackToStartMenuItem);

        menuBar.add(trackMenu);

        loopMenu.setMnemonic('L');
        loopMenu.setText("Loop");

        playLoopMenuItem.setAction(playLoopedAction);
        playLoopMenuItem.setText("Play");
        loopMenu.add(playLoopMenuItem);

        stopLoopMenuItem.setAction(stopLoopAction);
        stopLoopMenuItem.setText("Stop");
        stopLoopMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                stopLoopMenuItemActionPerformed(evt);
            }
        });
        loopMenu.add(stopLoopMenuItem);

        loopToStartMenuItem.setAction(loopToStartAction);
        loopToStartMenuItem.setText("To start");
        loopMenu.add(loopToStartMenuItem);

        previousLoopMenuItem.setAction(previousLoopAction);
        previousLoopMenuItem.setText("Previous");
        loopMenu.add(previousLoopMenuItem);

        nextLoopMenuItem.setAction(nextLoopAction);
        nextLoopMenuItem.setText("Next");
        loopMenu.add(nextLoopMenuItem);
        loopMenu.add(jSeparator1);

        newLoopMenuItem.setAction(newLoopAction);
        newLoopMenuItem.setText("New");
        loopMenu.add(newLoopMenuItem);

        duplicateMenuItem.setAction(duplicateLoopAction);
        duplicateMenuItem.setText("Duplicate");
        loopMenu.add(duplicateMenuItem);

        renameLoopMenuItem.setAction(renameLoopAction);
        renameLoopMenuItem.setText("Rename...");
        loopMenu.add(renameLoopMenuItem);

        deleteMenuItem.setAction(deleteLoopAction);
        deleteMenuItem.setText("Delete");
        loopMenu.add(deleteMenuItem);

        menuBar.add(loopMenu);

        jMenu1.setMnemonic('S');
        jMenu1.setText("Settings");

        jMenuItem1.setMnemonic('C');
        jMenuItem1.setText("Clear recent");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        menuBar.add(jMenu1);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mainTransportPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(loopPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainTransportPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(loopPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing

        try
        {
            if (saveHandler.handleWindowClosing().equals(Result.SUCCESS))
            {
                String path = saveHandler.getCurrentDirectory().getPath();
                preferences.put(Preference.filePath, path);
                core.releaseResources();
            }
        }
        catch (SaveStateOperationFailedException ex)
        {
        }

    }//GEN-LAST:event_formWindowClosing

    private void openMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_openMenuItemActionPerformed
    {//GEN-HEADEREND:event_openMenuItemActionPerformed
        core.stop();

        try
        {
            if (saveHandler.open().equals(Result.SUCCESS))
            {
                ((TitledBorder) mainTransportPanel.getBorder()).setTitle(core.getCurrentAudioFileName());
                //update recent files
                initialiseLoopSpinners();
                updateLoopBounds();
                updateLoopsList();
                setGUINotPlaying();
                updateTransport();
            }
        }
        catch (SaveStateOperationFailedException ex)
        {
        }
        catch (NoAudioFileLoadedException ex)
        {
            // Should not be possible.
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "No audio file loaded when updating transport bar.", ex);
            System.exit(0);
        }


    }//GEN-LAST:event_openMenuItemActionPerformed
    private void initialiseLoopSpinners() throws NoAudioFileLoadedException
    {
        updateLoopSpinnerModels();
        LoopBoundsFormatter formatter = new LoopBoundsFormatter();
        LoopStartVerifier startVerifier = new LoopStartVerifier();
        LoopEndVerifier endVerifier = new LoopEndVerifier();
        JFormattedTextField startField = new JFormattedTextField(formatter);
        startField.setInputVerifier(startVerifier);
        startField.addActionListener(startVerifier);
        JFormattedTextField endField = new JFormattedTextField(formatter);
        endField.setInputVerifier(endVerifier);
        endField.addActionListener(endVerifier);
        loopStartSpinner.setEditor(startField);
        loopEndSpinner.setEditor(endField);
    }


    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_exitMenuItemActionPerformed
    {//GEN-HEADEREND:event_exitMenuItemActionPerformed
        WindowEvent windowClosing = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        this.dispatchEvent(windowClosing);
    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void stopLoopMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_stopLoopMenuItemActionPerformed
    {//GEN-HEADEREND:event_stopLoopMenuItemActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_stopLoopMenuItemActionPerformed

    private void setSpeedButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_setSpeedButtonActionPerformed
    {//GEN-HEADEREND:event_setSpeedButtonActionPerformed
        try
        {
            core.setSpeedRatio((Double) speedRatioSpinner.getValue());
            updateLoopSpinnerModels();
            updateLoopBounds();
            updateLoopsList();
            updateTransport();
        }
        catch (SpeedRatioException ex)
        {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "Invalid speed ratio selected.", ex);
            System.exit(0);
        }
        catch (PlayModeException ex)
        {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "Cannot change speed ratio while playing.", ex);
            System.exit(0);
        }
        catch (NoAudioFileLoadedException ex)
        {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "No audio track loaded while trying to change speed ratio.", ex);
            System.exit(0);
        }
    }//GEN-LAST:event_setSpeedButtonActionPerformed

    private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_saveMenuItemActionPerformed
    {//GEN-HEADEREND:event_saveMenuItemActionPerformed
        try
        {
            saveHandler.save();
        }
        catch (SaveStateOperationFailedException ex)
        {
        }
    }//GEN-LAST:event_saveMenuItemActionPerformed

    private void saveAsMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_saveAsMenuItemActionPerformed
    {//GEN-HEADEREND:event_saveAsMenuItemActionPerformed
        try
        {
            saveHandler.saveAs();
        }
        catch (SaveStateOperationFailedException ex)
        {
        }
    }//GEN-LAST:event_saveAsMenuItemActionPerformed

    private void loopStartSpinnerStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_loopStartSpinnerStateChanged
    {//GEN-HEADEREND:event_loopStartSpinnerStateChanged
        try
        {
            core.setLoopStart(((SpinnerNumberModel) loopStartSpinner.getModel()).getNumber().intValue());
            updateLoopSpinnerModels();
            updateLoopBounds();
            updateTransport();
        }
        catch (LoopBoundsException ex)
        {
            // TODO MainFrame update spinner models when loop bounds change to prevent this from happening.
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "loop start spinner went out of bounds", ex);
            loopStartSpinner.getModel().setValue(0);

        }
        catch (NoAudioFileLoadedException ex)
        {
            //Shouldnt be possible
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "No audio file loaded when setting loop end spinner.", ex);
            System.exit(0);
        }
    }//GEN-LAST:event_loopStartSpinnerStateChanged

    private void loopEndSpinnerStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_loopEndSpinnerStateChanged
    {//GEN-HEADEREND:event_loopEndSpinnerStateChanged
        try
        {
            core.setLoopEnd(((SpinnerNumberModel) loopEndSpinner.getModel()).getNumber().intValue());
            updateLoopSpinnerModels();
            updateLoopBounds();
            updateTransport();
        }
        catch (LoopBoundsException ex)
        {
            // TODO MainFrame update spinner models when loop bounds change to prevent this from happening.
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "loop end spinner went out of bounds", ex);
            try
            {
                loopEndSpinner.getModel().setValue(core.getFrameCount());

            }
            catch (NoAudioFileLoadedException ex1)
            {
                //Shouldnt be possible
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "No audio file loaded when setting loop end spinner.", ex1);
                System.exit(0);
            }
        }
        catch (NoAudioFileLoadedException ex)
        {
            //Shouldnt be possible
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "No audio file loaded when setting loop end spinner.", ex);
            System.exit(0);
        }
    }//GEN-LAST:event_loopEndSpinnerStateChanged

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jMenuItem1ActionPerformed
    {//GEN-HEADEREND:event_jMenuItem1ActionPerformed
        saveHandler.clearRecentFiles();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private final Action playTrackAction = new AbstractAction()
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            try
            {
                if (playing)
                {
                    core.stop();
                    timer.stop();
                    currentTimeLabel.setText(convertSamplesToMinSecSamp(core.getCurrentFrameNumber()));
                    playing = false;
                    playTrackAction.putValue(NAME, "Play");
                    setGUINotPlaying();
                }
                else
                {
                    core.play();
                    timer.start();
                    playing = true;
                    playTrackAction.putValue(NAME, "Pause");
                    setGUIPlayingTrack();
                }
            }
            catch (LooperNotInitialisedException ex)
            {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "No file loaded", ex);
            }
        }
    };

    private void setLoopTransportEnabled(boolean enabled)
    {
        playLoopedButton.setEnabled(enabled);
    }

    private MouseListener transportClickedListener = new MouseListener()
    {

        @Override
        public void mouseClicked(MouseEvent e)
        {
            double fraction = (e.getX() * 1.0) / progressBarJPanel1.getWidth();
            int samplePosition;
            try
            {
                samplePosition = (int) Math.round(core.getFrameCount() * fraction);
                core.setCurrentFrameIndex(samplePosition);
                progressBarJPanel1.setCurrentPosition(samplePosition);
            }
            catch (NoAudioFileLoadedException ex)
            {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "No audio file loaded when transport click event happened.", ex);
                System.exit(0);
            }
            catch (LoopBoundsException | BufferBoundsException ex)
            {
                // Ignore clicks outside the loop bounds while in looping play.
                Logger.getLogger(MainFrame.class.getName()).log(Level.FINE, null, ex);
            }
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
        }

        @Override
        public void mouseEntered(MouseEvent e)
        {
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
        }
    };

    private final Action loadAudioFileAction = new AbstractAction()
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            try
            {
                if (saveHandler.checkForUnsavedChanges() == Result.CANCELLED)
                {
                    return;
                }

                fileChooser.setCurrentDirectory(saveHandler.getCurrentDirectory());

                fileChooser.setFileFilter(audioFileFilter);
                if (fileChooser.showOpenDialog(MainFrame.this) == JFileChooser.APPROVE_OPTION)
                {

                    core.startNewSaveFile(fileChooser.getSelectedFile());
                    TitledBorder border = (TitledBorder) mainTransportPanel.getBorder();
                    border.setTitle(core.getCurrentAudioFileName());
                    mainTransportPanel.repaint();
                    // Set current and total time counters
                    currentTimeLabel.setText(convertSamplesToMinSecSamp(0));
                    int totalFrames = core.getFrameCount();
                    totalTimeLabel.setText(convertSamplesToMinSecSamp(totalFrames));
                    initialiseLoopSpinners();
                    loopStartSpinner.setValue(core.getLoopStartIndex());
                    loopEndSpinner.setValue(core.getLoopEndIndex());
                    progressBarJPanel1.setCurrentPosition(0);
                    progressBarJPanel1.setMax(totalFrames);
                    
                    // enable buttons
                    setGUINotPlaying();
                    updateLoopsList();
                }
            }
            catch (AudioFileFailedToLoadException ex)
            {
                Logger.getLogger(MainFrame.class.getName()).log(Level.WARNING, "Loading failed", ex);
                JOptionPane.showMessageDialog(MainFrame.this, "The file failed to load: " + ex.getMessage());
            }
            catch (SaveStateOperationFailedException ex)
            {
            }
            catch (NoAudioFileLoadedException ex)
            {
                //Should not be possible
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "No audio file loaded after loading audio file.", ex);
                System.exit(0);
            }

        }

    };

    Action stopTrackAction = new AbstractAction()
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            core.stop();
            try
            {
                core.restartTrack();
            }
            catch (LoopBoundsException | BufferBoundsException ex)
            {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(MainFrame.this, "Failed to stop the track.");
                System.exit(0);
            }
            playTrackAction.putValue(NAME, "Play");
            setGUINotPlaying();
            try
            {
                updateTransport();
                currentTimeLabel.setText(String.valueOf(core.getCurrentFrameNumber()));
            }
            catch (NoAudioFileLoadedException ex)
            {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
            playing = false;

        }

    };

    private void updateTransport() throws NoAudioFileLoadedException
    {
        progressBarJPanel1.setMax(core.getFrameCount());
        progressBarJPanel1.setCurrentPosition(core.getCurrentFrameNumber());
        progressBarJPanel1.repaint();
        totalTimeLabel.setText(convertSamplesToMinSecSamp(core.getFrameCount()));
    }
    /**
     * The track level to start action. Called during non-looped play to restart
     * the track.
     */
    Action trackToStartAction = new AbstractAction()
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            try
            {
                core.restartTrack();
                progressBarJPanel1.setCurrentPosition(core.getCurrentFrameNumber());
                currentTimeLabel.setText(String.valueOf(core.getCurrentFrameNumber()));
            }
            catch (LoopBoundsException | BufferBoundsException ex)
            {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "Track to start request failed", ex);
                System.exit(0);
            }

        }
    };

    Action playLoopedAction = new AbstractAction()
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            try
            {
                if (playing)
                {
                    core.stop();
                    timer.stop();
                    currentTimeLabel.setText(convertSamplesToMinSecSamp(core.getCurrentFrameNumber()));
                    playing = false;
                    playLoopedAction.putValue(NAME, "Play");
                    setGUINotPlaying();
                }
                else
                {

                    core.playLooped();
                    progressBarJPanel1.setCurrentPosition((int) core.getLoopStartIndex());
                    timer.start();
                    playing = true;
                    playLoopedAction.putValue(NAME, "Pause");
                    setGUIPlayLooped();
                }
            }
            catch (LooperNotInitialisedException ex)
            {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "No file loaded", ex);
            }
        }

    };

    Action stopLoopAction = new AbstractAction()
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            playing = false;
            core.stop();
            try
            {
                core.restartLoop();
            }
            catch (PlayModeException ex)
            {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "In wrong play mode to restart loop.", ex);
                System.exit(0);
            }
            try
            {
                updateTransport();
            }
            catch (NoAudioFileLoadedException ex)
            {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "No audio file loaded when trying to stop loop.", ex);
                System.exit(0);
            }
            setGUINotPlaying();
        }
    };

    Action loopToStartAction = new AbstractAction()
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            try
            {
                core.restartLoop();
            }
            catch (PlayModeException ex)
            {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "Wrong play mode for loop to start action", ex);
                System.exit(0);
            }
        }
    };

    Action newSaveAction = new AbstractAction("newSave")
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            try
            {
                saveHandler.newSaveState();
                setGUIToNoSaveLoaded();
            }
            catch (SaveStateOperationFailedException ex)
            {
            }
        }

    };

    Action newLoopAction = new AbstractAction("newLoop")
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            String name = JOptionPane.showInputDialog(MainFrame.this, "Enter a name for the new loop.");
            core.addNewLoop(name);
            try
            {
                core.setCurrentLoop(name);
            }
            catch (PlayModeException ex)
            {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "Tried to change loop while playing loop", ex);
                System.exit(0);
            }
            updateLoopsList();
            updateLoopBounds();
            deleteLoopAction.setEnabled(true);
            nextLoopAction.setEnabled(true);
            previousLoopAction.setEnabled(true);
        }

    };

    Action duplicateLoopAction = new AbstractAction("duplicateLoop")
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            String newLoop = core.duplicateCurrentLoop();
            try
            {
                core.setCurrentLoop(newLoop);
            }
            catch (PlayModeException ex)
            {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "Tried to change loop as part of loop duplication action while playing loop", ex);
                System.exit(0);
            }
            updateLoopBounds();
            updateLoopsList();
            deleteLoopAction.setEnabled(true);
            nextLoopAction.setEnabled(true);
            previousLoopAction.setEnabled(true);
        }
    };

    Action renameLoopAction = new AbstractAction("rename")
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            core.renameCurrentLoop(JOptionPane.showInputDialog(MainFrame.this, "Enter a new name for the loop."));
            updateLoopsList();
        }
    };

    Action deleteLoopAction = new AbstractAction("deleteLoop")
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            ComboBoxModel model = loopListComboBoxExtra.getModel();
            // Don't delete the last loop.
            if (model.getSize() > 1)
            {
                // Select the next loop from the list to 
                // replace the deleted one.
                int itemIndex = loopListComboBoxExtra.getSelectedIndex();
                int maxIndex = loopListComboBoxExtra.getItemCount() - 1;
                String nextLoop;
                if (itemIndex == maxIndex)
                {
                    nextLoop = (String) loopListComboBoxExtra.getItemAt(itemIndex - 1);
                }
                else
                {
                    nextLoop = (String) loopListComboBoxExtra.getItemAt(itemIndex + 1);
                }
                try
                {
                    core.setCurrentLoop(nextLoop);
                }
                catch (PlayModeException ex)
                {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "Tried to change loop as part of delete loop action while playing loop", ex);
                    System.exit(0);
                }

                core.deleteLoop((String) loopListComboBoxExtra.getSelectedItem());

                updateLoopsList();
                updateLoopBounds();
                if (model.getSize() == 1)
                {
                    deleteLoopAction.setEnabled(false);
                    nextLoopAction.setEnabled(false);
                    previousLoopAction.setEnabled(false);
                }
            }
        }
    };

    Action nextLoopAction = new AbstractAction("nextLoop")
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {

            loopListComboBoxExtra.selectNextItem(true);
            try
            {
                updateLoopSpinnerModels();
            }
            catch (NoAudioFileLoadedException ex)
            {
                // Should not be possible
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "No audio file loaded when selecting next loop.", ex);
                System.exit(0);
            }

        }
    };

    Action previousLoopAction = new AbstractAction("previousLoop")
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            loopListComboBoxExtra.selectPreviousItem(true);
            try
            {
                updateLoopSpinnerModels();
            }
            catch (NoAudioFileLoadedException ex)
            {
                // Should not be possible
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "No audio file loaded when selecting next loop.", ex);
                System.exit(0);
            }
        }
    };

    Action setStartAction = new AbstractAction("setLoopStart")
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            long currentSample = core.getCurrentFrameNumber();
            try
            {
                core.setLoopStart((int) currentSample);
                updateLoopSpinnerModels();
                updateLoopBounds();
                updateTransport();
            }
            catch (LoopBoundsException ex)
            {
                Logger.getLogger(MainFrame.class.getName()).log(Level.WARNING, "Failed to set loop start.", ex);
            }
            catch (NoAudioFileLoadedException ex)
            {
                //Should not be possible
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "No audio loaded when setting loop start.", ex);
                System.exit(0);
            }
        }
    };

    Action setEndAction = new AbstractAction("setLoopEnd")
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            long currentSample = core.getCurrentFrameNumber();
            try
            {
                core.setLoopEnd((int) currentSample);
                updateLoopSpinnerModels();
                updateLoopBounds();
                updateTransport();
            }
            catch (LoopBoundsException ex)
            {
                Logger.getLogger(MainFrame.class.getName()).log(Level.WARNING, "Failed to set loop end.", ex);
            }
            catch (NoAudioFileLoadedException ex)
            {
                //Should not be possible
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "No audio loaded when setting loop end.", ex);
                System.exit(0);
            }
        }
    };

    private void updateLoopBounds()
    {
        ((JFormattedTextField) loopStartSpinner.getEditor()).setValue(core.getLoopStartIndex());
        ((JFormattedTextField) loopEndSpinner.getEditor()).setValue(core.getLoopEndIndex());
    }

    /**
     * Makes sure the loop bounds spinners models don't overlap to prevent
     * selecting start/endpoints before/after one another.
     */
    private void updateLoopSpinnerModels() throws NoAudioFileLoadedException
    {
        int startMax = Math.toIntExact(core.getLoopEndIndex()) - 1;
        SpinnerNumberModel startModel = new SpinnerNumberModel(core.getLoopStartIndex(), 0, startMax, 1);
        loopStartSpinner.setModel(startModel);

        int endMin = Math.toIntExact(core.getLoopStartIndex()) + 1;
        SpinnerNumberModel endModel = new SpinnerNumberModel(core.getLoopEndIndex(), endMin, core.getFrameCount(), 1);
        loopEndSpinner.setModel(endModel);
    }

    /**
     * The state immediately after startup before the user has selected an audio
     * file to load. All track and loop controls are disabled. User can only
     * open and existing save or create a new one.
     */
    private void setGUIToNoSaveLoaded()
    {

        enableRecentFileMenu();
        loopEndSpinner.setEnabled(false);
        loopStartSpinner.setEnabled(false);
        setSpeedButton.setEnabled(false);
        speedRatioSpinner.setEnabled(false);
        saveAsMenuItem.setEnabled(false);
        saveMenuItem.setEnabled(false);

        loopPanel.setEnabled(false);
        mainTransportPanel.setEnabled(false);

        playTrackAction.setEnabled(false);
        stopTrackAction.setEnabled(false);
        trackToStartAction.setEnabled(false);
        progressBarJPanel1.setEnabled(false);
        progressBarJPanel1.removeMouseListener(transportClickedListener);

        playLoopedAction.setEnabled(false);
        stopLoopAction.setEnabled(false);
        loopToStartAction.setEnabled(false);
        loopListComboBoxExtra.setEnabled(false);
        newLoopAction.setEnabled(false);
        duplicateLoopAction.setEnabled(false);
        renameLoopAction.setEnabled(false);
        deleteLoopAction.setEnabled(false);
        previousLoopAction.setEnabled(false);
        nextLoopAction.setEnabled(false);
        setStartAction.setEnabled(false);
        setEndAction.setEnabled(false);

    }

    /**
     * The GUI appearance after a savefile has been loaded when not playing.
     */
    private void setGUINotPlaying()
    {
        enableRecentFileMenu();
        loopStartSpinner.setEnabled(true);
        loopEndSpinner.setEnabled(true);
        setSpeedButton.setEnabled(true);
        speedRatioSpinner.setEnabled(true);
        saveAsMenuItem.setEnabled(true);
        saveMenuItem.setEnabled(true);

        loopPanel.setEnabled(true);
        playLoopedAction.setEnabled(true);
        stopLoopAction.setEnabled(true);
        loopToStartAction.setEnabled(true);
        loopListComboBoxExtra.setEnabled(true);
        newLoopAction.setEnabled(true);
        duplicateLoopAction.setEnabled(true);
        setStartAction.setEnabled(true);
        setEndAction.setEnabled(true);

        boolean moreThanOneLoop = loopListComboBoxExtra.getModel().getSize() > 1;
        deleteLoopAction.setEnabled(moreThanOneLoop);
        previousLoopAction.setEnabled(moreThanOneLoop);
        nextLoopAction.setEnabled(moreThanOneLoop);

        renameLoopAction.setEnabled(true);

        mainTransportPanel.setEnabled(true);
        playTrackAction.setEnabled(true);
        stopTrackAction.setEnabled(true);
        trackToStartAction.setEnabled(true);
        progressBarJPanel1.setEnabled(true);
        progressBarJPanel1.addMouseListener(transportClickedListener);

        playTrackAction.putValue(Action.NAME, "Play");
        playLoopedAction.putValue(Action.NAME, "Loop");
    }

    private void setGUIPlayingTrack()
    {
        enableRecentFileMenu();
        setSpeedButton.setEnabled(false);
        speedRatioSpinner.setEnabled(false);
        playLoopedAction.setEnabled(false);
        stopLoopAction.setEnabled(false);
        loopToStartAction.setEnabled(false);

        loopListComboBoxExtra.setEnabled(true);
        newLoopAction.setEnabled(true);
        duplicateLoopAction.setEnabled(true);
        renameLoopAction.setEnabled(true);
        if (loopListComboBoxExtra.getModel().getSize() > 1)
        {
            deleteLoopAction.setEnabled(true);
            previousLoopAction.setEnabled(true);
            nextLoopAction.setEnabled(true);
        }
        setStartAction.setEnabled(true);
        setEndAction.setEnabled(true);
    }

    private void setGUIPlayLooped()
    {
        enableRecentFileMenu();
        loopStartSpinner.setEnabled(true);
        loopEndSpinner.setEnabled(true);
        setSpeedButton.setEnabled(false);
        speedRatioSpinner.setEnabled(false);
        playTrackAction.setEnabled(false);
        trackToStartAction.setEnabled(false);
        stopTrackAction.setEnabled(false);

        duplicateLoopAction.setEnabled(false);
        newLoopAction.setEnabled(false);
        previousLoopAction.setEnabled(false);
        nextLoopAction.setEnabled(false);
        deleteLoopAction.setEnabled(false);
        loopListComboBoxExtra.setEnabled(false);

    }

    private void updateLoopsList()
    {
        loopListComboBoxExtra.removeActionListener(loopListComboBoxListener);
        loopListComboBoxExtra.removeAllItems();
        for (String eachName : core.getLoopNames())
        {
            loopListComboBoxExtra.addItem(eachName);
        }
        loopListComboBoxExtra.setSelectedItem(core.getCurrentLoopName());
        loopListComboBoxExtra.addActionListener(loopListComboBoxListener);
    }

    public String convertSamplesToMinSecSamp(long samples)
    {
        int sampleRate = (int) core.getSampleRate();
        int samplesDisplay = (int) (samples % sampleRate);
        int totalSeconds = (int) Math.floor(samples / sampleRate);
        int secondsDisplay = totalSeconds % 60;
        int totalMinutes = totalSeconds / 60;
        // Pad out seconds
        String secondsString = String.valueOf(secondsDisplay);
        int secondsDigits = secondsString.length();
        int zerosToAdd = 2 - secondsDigits;
        for (int i = 0; i < zerosToAdd; i++)
        {
            secondsString = "0" + secondsString;
        }

        String sampleRateString = String.valueOf(sampleRate);
        int sampleRateDigits = sampleRateString.length();
        String samplesDisplayString = String.valueOf(samplesDisplay);
        int currentSampleDigits = samplesDisplayString.length();
        zerosToAdd = sampleRateDigits - currentSampleDigits;
        for (int i = 0; i < zerosToAdd; i++)
        {
            samplesDisplayString = "0" + samplesDisplayString;
        }

        return totalMinutes + ":" + secondsString + ":" + samplesDisplayString;

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try
        {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }
        catch (ClassNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (InstantiationException ex)
        {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (IllegalAccessException ex)
        {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (javax.swing.UnsupportedLookAndFeelException ex)
        {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                new MainFrame().setVisible(true);
            }
        });
    }

    /**
     * Draws the loop bounds overlay on top of the transport bar.
     */
    class TransportBarLayerUI extends LayerUI<JComponent>
    {

        @Override
        public void paint(Graphics g, JComponent c)
        {
            super.paint(g, c);

            int totalSamples;
            try
            {
                totalSamples = core.getFrameCount();
                if (totalSamples != 0)
                {
                    Graphics2D g2D = (Graphics2D) g.create();
                    int leftPx = (int) (progressBarJPanel1.getWidth() * (1.0 * core.getLoopStartIndex()) / totalSamples);
                    int rightPx = (int) (progressBarJPanel1.getWidth() * (1.0 * core.getLoopEndIndex()) / totalSamples);
                    int loopWidth = rightPx - leftPx;
                    g2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
                    g2D.setPaint(new Color(0.5f, 0.5f, 0.5f));
                    g2D.fillRect(leftPx, 0, loopWidth, progressBarJPanel1.getHeight());
                    g2D.dispose();
                }
            }
            catch (NoAudioFileLoadedException ex)
            {
                totalSamples = 0;
            }

        }
    }

    public class LoopBoundsFormatter extends JFormattedTextField.AbstractFormatter
    {

        Pattern pattern;
        Matcher matcher;
        MainFrame mainFrame;

        public LoopBoundsFormatter()
        {
            int sampleDigits = String.valueOf((int) core.getSampleRate()).length();
            String regex = "^(\\d)+:(\\d){2}:(\\d){" + sampleDigits + "}$";
            pattern = Pattern.compile(regex);
        }

        @Override
        public Object stringToValue(String text) throws ParseException
        {
            // Check pattern matches
            CharSequence sequence = text.subSequence(0, text.length());
            matcher = pattern.matcher(sequence);

            if (matcher.find())
            {
                // Text is correctly formatted
                String[] parts = text.split(":");
                int frame = 0;
                // add mins
                frame += Integer.parseInt(parts[0]) * 60;
                // add secs
                frame += Integer.parseInt(parts[1]);
                // convert seconds to frames
                frame *= core.getSampleRate();
                // add samples
                frame += Integer.parseInt(parts[2]);

                return frame;
            }
            else
            {
                // Match was not found text incorrectly formatted
                throw new ParseException("Input did not match regexp pattern.", 0);
            }

        }

        @Override
        public String valueToString(Object value) throws ParseException
        {
            if (value == null)
            {
                return "";
            }
            return convertSamplesToMinSecSamp(Long.parseLong(value.toString()));
        }

    }

    private class LoopStartVerifier extends InputVerifier implements ActionListener
    {

        @Override
        public boolean shouldYieldFocus(JComponent input)
        {
            return verify(input);
        }

        @Override
        public boolean verify(JComponent input)
        {
            JFormattedTextField textField = (JFormattedTextField) input;
            int requestedValue = Integer.parseInt(textField.getValue().toString());
            try
            {
                boolean inputValid = requestedValue <= core.getFrameCount();

                if (inputValid)
                {
                    try
                    {
                        core.setLoopStart(requestedValue);
                        updateLoopSpinnerModels();
                        updateLoopBounds();
                        updateTransport();
                        return true;
                    }
                    catch (LoopBoundsException ex)
                    {
                        Logger.getLogger(MainFrame.class.getName()).log(Level.WARNING, "User requested out of range loop bounds.", ex);
                        JOptionPane.showMessageDialog(MainFrame.this, "Invalid time value: " + ((JFormattedTextField) input).getText() + ". The loops start time must be between the start of the track and the loop end time.", "Loop bounds out of range.", JOptionPane.YES_OPTION);
                        textField.setValue(core.getLoopStartIndex());
                    }
                }
                else
                {
                    JOptionPane.showMessageDialog(MainFrame.this, "Invalid time value: " + ((JFormattedTextField) input).getText() + ". The loops start time must be between the start of the track and the loop end time.", "Invalid time value", JOptionPane.YES_OPTION);
                    textField.setValue(core.getLoopStartIndex());
                }
            }
            catch (NoAudioFileLoadedException ex)
            {
                //  Should not happen
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "No audio file loaded when verifying loop bounds input.", ex);
                System.exit(0);
            }
            return false;
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            verify((JComponent) e.getSource());
        }

    }

    private class LoopEndVerifier extends InputVerifier implements ActionListener
    {

        @Override
        public boolean shouldYieldFocus(JComponent input)
        {
            return verify(input); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public boolean verify(JComponent input)
        {
            JFormattedTextField textField = (JFormattedTextField) input;
            int requestedValue = Integer.parseInt(textField.getValue().toString());
            try
            {
                boolean inputValid = requestedValue <= core.getFrameCount();

                if (inputValid)
                {
                    try
                    {
                        core.setLoopEnd(requestedValue);
                        updateLoopSpinnerModels();
                        updateLoopBounds();
                        updateTransport();
                        return true;
                    }
                    catch (LoopBoundsException ex)
                    {
                        Logger.getLogger(MainFrame.class.getName()).log(Level.WARNING, "User requested out of range loop bounds.", ex);
                        JOptionPane.showMessageDialog(MainFrame.this, "Invalid time value: " + ((JFormattedTextField) input).getText() + ". The loops end time must be after its start time and before the end of the track.", "Loop bounds out of range.", JOptionPane.YES_OPTION);
                        textField.setValue(core.getLoopEndIndex());
                    }
                }
                else
                {
                    JOptionPane.showMessageDialog(MainFrame.this, "Invalid time value: " + ((JFormattedTextField) input).getText() + ". The loops end time must be after its start time and before the end of the track.", "Invalid time value", JOptionPane.YES_OPTION);
                    textField.setValue(core.getLoopEndIndex());
                }
            }
            catch (NoAudioFileLoadedException ex)
            {
                //  Should not happen
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "No audio file loaded when verifying loop bounds input.", ex);
                System.exit(0);
            }
            return false;
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            verify((JComponent) e.getSource());
        }

    }

    private void updateRecentFilesMenu()
    {
        recentMenu.removeAll();
        for (JMenuItem eachFile : saveHandler.getRecentFileJMenuItems())
        {
            recentMenu.add(eachFile);
        }
    }

    RecentFilesUpdateListener recentFilesUpdateListener = new RecentFilesUpdateListener()
    {

        @Override
        public void update()
        {
            updateRecentFilesMenu();
        }
    };

    OpenRecentFileListener openRecentFileListener = new OpenRecentFileListener()
    {

        @Override
        public void openRecent()
        {
            try
            {
                ((TitledBorder) mainTransportPanel.getBorder()).setTitle(core.getCurrentAudioFileName());
                //update recent files
                initialiseLoopSpinners();
                updateLoopBounds();
                updateLoopsList();
                setGUINotPlaying();
                updateTransport();
            }
            catch (NoAudioFileLoadedException ex)
            {
                // should never happen
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, "No audio file loaded after opening recent file", ex);
                throw new IllegalStateException("No audio file loaded after opening recent file");
            }
        }
    };
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel centerLoopPanel;
    private javax.swing.JLabel currentTimeLabel;
    private javax.swing.JButton deleteLoopButton;
    private javax.swing.JMenuItem deleteMenuItem;
    private javax.swing.JButton duplicateLoopButton;
    private javax.swing.JMenuItem duplicateMenuItem;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPanel leftLoopPanel;
    private javax.swing.JSpinner loopEndSpinner;
    private uk.co.jmsoft.comboboxextra.ComboBoxExtra loopListComboBoxExtra;
    private javax.swing.JMenu loopMenu;
    private javax.swing.JPanel loopPanel;
    private javax.swing.JSpinner loopStartSpinner;
    private javax.swing.JMenuItem loopToStartMenuItem;
    private javax.swing.JPanel mainTransportPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JButton newLoopButton;
    private javax.swing.JMenuItem newLoopMenuItem;
    private javax.swing.JMenuItem newSaveMenuItem;
    private javax.swing.JButton nextLoopButton;
    private javax.swing.JMenuItem nextLoopMenuItem;
    private javax.swing.JMenuItem openMenuItem;
    private javax.swing.JMenuItem playLoopMenuItem;
    private javax.swing.JButton playLoopedButton;
    private javax.swing.JButton playTrackButton;
    private javax.swing.JMenuItem playTrackMenuItem;
    private javax.swing.JButton previousLoopButton;
    private javax.swing.JMenuItem previousLoopMenuItem;
    private uk.co.jmsoft.progressbarjpanel.ProgressBarJPanel progressBarJPanel1;
    private javax.swing.JMenu recentMenu;
    private javax.swing.JMenuItem renameLoopMenuItem;
    private javax.swing.JPanel rightLoopPanel;
    private javax.swing.JMenuItem saveAsMenuItem;
    private javax.swing.JMenuItem saveMenuItem;
    private javax.swing.JButton setLoopEndButton;
    private javax.swing.JButton setLoopStartButton;
    private javax.swing.JButton setSpeedButton;
    private javax.swing.JLabel speedRatioLabel;
    private javax.swing.JSpinner speedRatioSpinner;
    private javax.swing.JMenuItem stopLoopMenuItem;
    private javax.swing.JButton stopTrackButton;
    private javax.swing.JMenuItem stopTrackMenuItem;
    private javax.swing.JLabel totalTimeLabel;
    private javax.swing.JMenu trackMenu;
    private javax.swing.JButton trackToStartButton;
    private javax.swing.JMenuItem trackToStartMenuItem;
    private javax.swing.JPanel transportBarPanel;
    // End of variables declaration//GEN-END:variables

}
