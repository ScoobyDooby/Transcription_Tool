/*
 * The MIT License
 *
 * Copyright 2017 John Marshall.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uk.co.jmsoft.transcriptiontool.gui;

import java.io.File;
import javax.sound.sampled.AudioFileFormat.Type;
import javax.sound.sampled.AudioSystem;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Scooby
 */
public class AudioFileFilter extends FileFilter
{

    @Override
    public boolean accept(File f)
    {
        boolean isDirectory = f.isDirectory();
        boolean isAudioFile = false;
        
        
        Type[] types = AudioSystem.getAudioFileTypes();
        for(Type eachType : types)
        {
            if(f.getName().endsWith("." + eachType.getExtension()))
            {
                isAudioFile = true;
            }
        }
        
        return isDirectory || isAudioFile;
    }

    @Override
    public String getDescription()
    {
        Type[] types = AudioSystem.getAudioFileTypes();
        String result = "Audio files";
        for(Type eachType : types)
        {
            result += ", *." + eachType.getExtension();
        }
        return result;
    }
    
}
