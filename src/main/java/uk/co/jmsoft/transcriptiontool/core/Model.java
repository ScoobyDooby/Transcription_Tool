/*
 * The MIT License
 *
 * Copyright 2017 John Marshall.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uk.co.jmsoft.transcriptiontool.core;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.WaveformSimilarityBasedOverlapAdd;
import be.tarsos.dsp.io.jvm.AudioDispatcherFactory;
import uk.co.jmsoft.transcriptiontool.exceptions.PlayModeException;
import uk.co.jmsoft.transcriptiontool.exceptions.BufferBoundsException;
import uk.co.jmsoft.transcriptiontool.exceptions.NoAudioFileLoadedException;
import uk.co.jmsoft.transcriptiontool.exceptions.LooperNotInitialisedException;
import uk.co.jmsoft.transcriptiontool.exceptions.AudioFileFailedToLoadException;
import uk.co.jmsoft.transcriptiontool.exceptions.LoopBoundsException;
import uk.co.jmsoft.transcriptiontool.exceptions.SpeedRatioException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioFormat.Encoding;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;
import uk.co.jmsoft.savehandler.SaveStateManager;
import uk.co.jmsoft.savehandler.exceptions.SaveStateOperationFailedException;

/**
 * The Model represents the state of the audio player.
 * @author Scooby
 */
public class Model implements SaveStateManager
{

    AudioInputStream inputStream;
    AudioFormat audioFormat;
    SourceDataLine sourceDataLine;
    private final Player player = new Player();
    private SaveFile currentSaveObject = new SaveFile();
    private boolean hasUnsavedChanges = false;

    private double speedRatio = 1.0;

    /**
     * The loops to be used when the track has been slowed. Take into account
     * the new length of the track.
     */
    private final Map<String, Loop> slowedLoops = new HashMap<>();

    /**
     * The audio data.
     */
    private byte[] buffer;
    /**
     * The slowed audio data.
     */
    private byte[] slowedAudio;

    public double getSpeedRatio()
    {
        return speedRatio;
    }

    public void setSpeedRatio(double speedRatio) throws SpeedRatioException, PlayModeException, NoAudioFileLoadedException
    {

        if (this.speedRatio == speedRatio)
        {
            //Nothing to do.
            return;
        }
        if (player.isPlaying())
        {
            throw new PlayModeException("Can't change speed ratio while playing.", Player.PlayMode.PLAY);
        }
        // Limit speed ratio to reasonable values.
        if (speedRatio < 0.1 || speedRatio > 2)
        {
            throw new SpeedRatioException("Speed ratio was outside allowed range of 0.1 to 2 : " + speedRatio);
        }
        this.speedRatio = speedRatio;
        if (speedRatio == 1.0)
        {
            // use the original track and loops.
            slowedAudio = null;
            slowedLoops.clear();
            String currentLoop = player.getCurrentLoopName();
            int totalFrames = Math.toIntExact(bytesToFrames(buffer.length, audioFormat));
            player.initialise(buffer, audioFormat, totalFrames, sourceDataLine, currentSaveObject.getLoop(currentLoop));
        }
        else
        {
            // check if track is mono or stereo
            boolean isMono = audioFormat.getChannels() == 1;
            if (isMono)
            {
                try
                {
                    // 1. Slow audio
                    slowedAudio = slowAudio(buffer, audioFormat);
                    // 2. Scale loops

                    String currentLoopName = player.getCurrentLoopName();
                    recalculateSlowLoops();

                    // 3. Reinitialise player with new audio data and loop.
                    int totalFrames = Math.toIntExact(bytesToFrames(slowedAudio.length, audioFormat));
                    player.initialise(slowedAudio, audioFormat, totalFrames, sourceDataLine, slowedLoops.get(currentLoopName));

                }
                catch (UnsupportedAudioFileException ex)
                {
                    // This should never happen as audio file type has already been checked.
                    Logger.getLogger(Model.class.getName()).log(Level.SEVERE, "Audio file type not supported by TarsosDSP", ex);
                    System.exit(0);
                }

            }
            else // is stereo
            {
                // separate stereo channels
                byte[] leftBuffer = new byte[buffer.length / 2];
                byte[] rightBuffer = new byte[buffer.length / 2];

                int frameSize = audioFormat.getFrameSize();
                int halfFrameSize = frameSize / 2;
                int monoCount = 0;
                for (int i = 0; i < buffer.length; i += frameSize)
                {
                    for (int j = 0; j < halfFrameSize; j++)
                    {
                        leftBuffer[j + monoCount] = buffer[i + j];
                        rightBuffer[j + monoCount] = buffer[i + j + halfFrameSize];
                    }
                    monoCount += halfFrameSize;
                }

                try
                {
                    // slow channels
                    Encoding encoding = audioFormat.getEncoding();
                    float sampleRate = audioFormat.getSampleRate();
                    // Mono frame size is half stereo.
                    int monoFrameSize = audioFormat.getFrameSize() / 2;
                    int monoSampleSize = audioFormat.getSampleSizeInBits();
                    int channels = 1;
                    boolean bigEndian = audioFormat.isBigEndian();
                    AudioFormat monoFormat = new AudioFormat(encoding, sampleRate, monoSampleSize, channels, monoFrameSize, sampleRate, bigEndian, audioFormat.properties());

                    leftBuffer = slowAudio(leftBuffer, monoFormat);
                    rightBuffer = slowAudio(rightBuffer, monoFormat);
                    // recombine to stereo
                    int totalLength = leftBuffer.length + rightBuffer.length;
                    int monoIndex = 0;
                    slowedAudio = new byte[totalLength];
                    for (int i = 0; i < totalLength; i += frameSize)
                    {
                        for (int j = 0; j < halfFrameSize; j++)
                        {
                            slowedAudio[i + j] = leftBuffer[monoIndex];
                            slowedAudio[i + j + halfFrameSize] = rightBuffer[monoIndex];
                            monoIndex++;
                        }
                    }
                    // Reinitialise player with new audio data and loops.
                    int totalFrames = Math.toIntExact(bytesToFrames(slowedAudio.length, audioFormat));
                    String currentLoopName = player.getCurrentLoopName();
                    recalculateSlowLoops();
                    player.initialise(slowedAudio, audioFormat, totalFrames, sourceDataLine, slowedLoops.get(currentLoopName));
                }
                catch (UnsupportedAudioFileException ex)
                {
                    // This should never happen as audio file type has already been checked.
                    Logger.getLogger(Model.class.getName()).log(Level.SEVERE, "Audio file type not supported by TarsosDSP", ex);
                    System.exit(0);
                }

            }
        }
    }

    /**
     * Applies the current speedRatio to the samples in the input buffer.
     * Returns a new byte array containing the slowed data.
     * <p>
     * @param inputBuffer
     *                    <p>
     * @return
     */
    private byte[] slowAudio(byte[] inputBuffer, AudioFormat audioFormat) throws UnsupportedAudioFileException
    {
        WaveformSimilarityBasedOverlapAdd wsola;
        // TODO Player - experiment with different parameters for slowdown processor.
        wsola = new WaveformSimilarityBasedOverlapAdd(WaveformSimilarityBasedOverlapAdd.Parameters.slowdownDefaults(speedRatio, audioFormat.getSampleRate()));
        AudioDispatcher audioDispatcher;
        ByteArrayWriterAudioProcessor byteArrayWriterAudioProcessor = new ByteArrayWriterAudioProcessor();
        audioDispatcher = AudioDispatcherFactory.fromByteArray(inputBuffer, audioFormat, wsola.getInputBufferSize(), wsola.getOverlap());
        wsola.setDispatcher(audioDispatcher);
        audioDispatcher.addAudioProcessor(wsola);
        audioDispatcher.addAudioProcessor(byteArrayWriterAudioProcessor);
        audioDispatcher.run();
        return byteArrayWriterAudioProcessor.getOutputByteArray();
    }

    /**
     * Clears and re-populates the slowed loop list.
     */
    private void recalculateSlowLoops()
    {
        slowedLoops.clear();
        for (String eachLoop : currentSaveObject.getLoops().keySet())
        {
            Loop tempLoop = currentSaveObject.getLoops().get(eachLoop).copy(eachLoop);
            tempLoop.setStartFrameIndex((int) (tempLoop.getStartFrameIndex() * (1 / speedRatio)));
            tempLoop.setEndFrameIndex((int) (tempLoop.getEndFrameIndex() * (1 / speedRatio)));
            slowedLoops.put(eachLoop, tempLoop);
        }
    }

    /**
     * Sets the currently selected loop by name.
     * <p>
     * @param name
     *             <p>
     * @throws LooperNotInitialisedException
     * @throws PlayModeException
     */
    public void chooseLoop(String name) throws LooperNotInitialisedException, PlayModeException
    {
        if (speedRatio == 1.0)
        {
            player.setLoop(currentSaveObject.getLoops().get(name));
        }
        else
        {
            player.setLoop(slowedLoops.get(name));
        }

    }

    /**
     * Starts play in non looping mode.
     * <p>
     * @throws LooperNotInitialisedException
     */
    public void play() throws LooperNotInitialisedException
    {
        player.play();
    }

    /**
     * Stops play.
     */
    public void stop()
    {
        player.stop();
    }

    /**
     * Sets the sample to start the loop on during looped play. Must be before
     * the sample currently assigned as the end of the loop, otherwise throws an
     * exception. This index is relative to the whole track.
     * <p>
     * @param sampleNumber
     *                     <p>
     * @throws LoopBoundsException
     */
    public void setLoopStart(int sampleNumber) throws LoopBoundsException
    {
        // Make sure loop start is before end
        if (!(sampleNumber < player.getCurrentLoop().getEndFrameIndex()))
        {
            throw new LoopBoundsException();
        }
        else
        {

            if (sampleNumber > player.getCurrentFrameIndex())
            {
                try
                {
                    // Current index is before the start of the loop so restart the loop.
                    player.setCurrentFrameIndex(sampleNumber);
                }
                catch (BufferBoundsException ex)
                {
                    throw new LoopBoundsException();
                }
            }
            else
            {
                // Current index still in range so no need to adjust index.
            }
            // Adjust loops.

            player.getCurrentLoop().setStartFrameIndex(sampleNumber);
            if (speedRatio != 1.0)
            {
                currentSaveObject.getLoop(player.getCurrentLoopName()).setStartFrameIndex((int) (sampleNumber * speedRatio));
            }

        }
        hasUnsavedChanges = true;
    }

    /**
     * Sets the currently selected loops last frame/sample index. This index is
     * relative to the whole track.
     *
     * @param sampleNumber
     *                     <p>
     * @throws LoopBoundsException
     */
    public void setLoopEnd(int sampleNumber) throws LoopBoundsException
    {
        // Make sure loop start is before end
        if (!(sampleNumber > player.getCurrentLoop().getStartFrameIndex()))
        {
            throw new LoopBoundsException();
        }
        else
        {
            // Looping so local current frame index must stay within loop bounds.
            if (player.getCurrentFrameIndex() > sampleNumber)
            {
                try
                {
                    // Current index is past the end loop so restart the loop
                    player.setCurrentFrameIndex(player.getCurrentLoop().getStartFrameIndex());
                }
                catch (BufferBoundsException ex)
                {
                    throw new LoopBoundsException(ex);
                }
            }
            else
            {
                // Current index still in range so don't need to change it as it is only relative to the start point.

            }
            // Adjust loops.

            player.getCurrentLoop().setEndFrameIndex(sampleNumber);
            if (speedRatio != 1.0)
            {
                currentSaveObject.getLoop(player.getCurrentLoopName()).setEndFrameIndex((int) (sampleNumber * speedRatio));
            }
        }
        hasUnsavedChanges = true;
    }

    /**
     * Returns the track length in frames/samples.
     * <p>
     * @return
     *         <p>
     * @throws NoAudioFileLoadedException
     */
    public int getFrameCount() throws NoAudioFileLoadedException
    {
        return player.getTotalFrames();
    }

    /**
     * Returns the current position of play within the track in frames/samples.
     * <p>
     * @return
     */
    public int getCurrentFrameNumber()
    {
        return player.getCurrentFrameIndex();
    }

    /**
     * Sets the current position of play within the track in frames/samples.
     * <p>
     * @param frameIndex
     *                   <p>
     * @throws LoopBoundsException
     * @throws BufferBoundsException
     */
    public void setCurrentFrameIndex(int frameIndex) throws LoopBoundsException, BufferBoundsException
    {
        player.setCurrentFrameIndex(frameIndex);
    }

    /**
     * Starts play in looped mode.
     * <p>
     * @throws LooperNotInitialisedException
     */
    public void playLooped() throws LooperNotInitialisedException
    {
        player.playLooped();
    }

    public void restartLoop() throws PlayModeException
    {
        player.sendTransportToLoopStart();
    }

    /**
     * Gets the index of the last frame/sample in the currently selected loop.
     * This index is relative to the whole track.
     * <p>
     * @return
     */
    public long getLoopEndIndex()
    {
        return player.getEndFrameIndex();
    }

    /**
     * Gets the index of the first frame/sample in the currently selected loop.
     * This index is relative to the whole track.
     * <p>
     * @return
     */
    public long getLoopStartIndex()
    {
        return player.getStartFrameIndex();
    }

    /**
     * Clean up method for the core object.
     */
    public void releaseResources()
    {
        if (inputStream != null)
        {
            try
            {
                inputStream.close();

            }
            catch (IOException ex)
            {
                Logger.getLogger(Model.class
                        .getName()).log(Level.SEVERE, "Failed to close input stream.", ex);
            }
        }
        if (sourceDataLine != null)
        {
            sourceDataLine.close();
        }
    }

    /**
     * Closes the currently open savefile and creates and loads a new savefile
     * based on the user supplied audio file.<br>
     * Adds a single loop and sets it to loop over the entire track.<br>
     * Resets the transport to the start of the track.<br>
     * <p>
     * @param audioFile
     *                  <p>
     * @throws AudioFileFailedToLoadException
     */
    public void startNewSaveFile(File audioFile) throws AudioFileFailedToLoadException
    {
        Logger.getLogger(Model.class.getName()).log(Level.INFO, "Starting new save file for audio file: {0}", audioFile.getPath());
        try
        {
            newSaveState();
            currentSaveObject = new SaveFile();
            currentSaveObject.setAudioFile(audioFile);
            currentSaveObject.addNewLoop("Loop one");
            if (inputStream != null)
            {
                inputStream.close();
            }

            inputStream = AudioSystem.getAudioInputStream(audioFile);

            currentSaveObject.getLoop("Loop one").setEndFrameIndex(Math.toIntExact(inputStream.getFrameLength()));
            audioFormat = inputStream.getFormat();
            int frameLength = Math.toIntExact(inputStream.getFrameLength());
            buffer = new byte[Math.toIntExact(framesToBytes(frameLength, audioFormat))];
            // read in all the samples from the file.
            int read = 0;
            while (inputStream.read(buffer, read, 1000) >= 0)
            {
                read += 1000;

            }
            Logger.getLogger(Model.class
                    .getName()).log(Level.FINE, "Audio file read to buffer OK: {0} bytes.", read);

            if (sourceDataLine
                    != null)
            {
                sourceDataLine.close();
            }
            DataLine.Info lineInfo = new DataLine.Info(SourceDataLine.class, audioFormat);
            Logger.getLogger(Model.class.getName()).log(Level.INFO, "Getting system mixers:");
            Mixer.Info[] info = AudioSystem.getMixerInfo();
            for(Mixer.Info eachInfo: info)
            {
                
                Logger.getLogger(Model.class.getName()).log(Level.INFO, "Mixer: {0}", eachInfo.toString());
            }
            sourceDataLine = (SourceDataLine)AudioSystem.getLine(lineInfo);

            Logger.getLogger(Model.class
                    .getName()).log(Level.FINE, "Source dataline created OK");

            sourceDataLine.open(audioFormat, Math.toIntExact(audioFormat.getFrameSize() * 10000));
            Logger.getLogger(Model.class
                    .getName()).log(Level.FINE, "Source dataline opened OK");

            player.initialise(buffer, audioFormat, frameLength, sourceDataLine, currentSaveObject.getLoops().get("Loop one"));

        }
        catch (IllegalArgumentException| LineUnavailableException | UnsupportedAudioFileException | IOException ex)
        {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
            throw new AudioFileFailedToLoadException(ex);
        }
        hasUnsavedChanges = false;
    }

    public static long bytesToFrames(long byteCount, AudioFormat format)
    {
        return byteCount / format.getFrameSize();
    }

    public static long framesToBytes(long sampleCount, AudioFormat format)
    {
        return sampleCount * format.getFrameSize();
    }

    public String getCurrentAudioFileName()
    {
        return currentSaveObject.getAudioFile().getAbsolutePath();
    }

    public void restartTrack() throws LoopBoundsException, BufferBoundsException
    {
        player.setCurrentFrameIndex(0);
    }

    public void setCurrentLoop(String loopName) throws PlayModeException
    {
        if (speedRatio == 1.0)
        {
            player.setLoop(currentSaveObject.getLoops().get(loopName));
        }
        else
        {
            player.setLoop(slowedLoops.get(loopName));
        }
    }

    public String[] getLoopNames()
    {
        Set<String> keySet = currentSaveObject.getLoops().keySet();
        return keySet.toArray(new String[keySet.size()]);
    }

    public void addNewLoop(String name)
    {
        // Add new unslowed loop to savefile
        currentSaveObject.addNewLoop(name);
        currentSaveObject.getLoop(name).setEndFrameIndex(Math.toIntExact(bytesToFrames(buffer.length, audioFormat)));
        // Create slowed loop if nessesary.
        if (speedRatio != 1.0)
        {
            Loop tempLoop = currentSaveObject.getLoop(name).copy(name);
            tempLoop.setStartFrameIndex((int) (tempLoop.getStartFrameIndex() * (1 / speedRatio)));
            tempLoop.setEndFrameIndex((int) (tempLoop.getEndFrameIndex() * (1 / speedRatio)));
            slowedLoops.put(name, tempLoop);
        }
        hasUnsavedChanges = true;
    }

    /**
     * Duplicates the current loop and returns the name of the new loop.
     * <p>
     * @return
     */
    public String duplicateCurrentLoop()
    {
        Loop newLoop = currentSaveObject.getLoop(player.getCurrentLoopName()).copy("Copy of " + player.getCurrentLoopName());
        currentSaveObject.addLoop(newLoop);
        if (speedRatio != 1.0)
        {
            newLoop = newLoop.copy(newLoop.getName());
            newLoop.setStartFrameIndex((int) (newLoop.getStartFrameIndex() * (1 / speedRatio)));
            newLoop.setEndFrameIndex((int) (newLoop.getEndFrameIndex() * (1 / speedRatio)));
            slowedLoops.put(newLoop.getName(), newLoop);
        }
        hasUnsavedChanges = true;
        return newLoop.getName();
    }

    public void renameCurrentLoop(String newName)
    {
        currentSaveObject.renameLoop(player.getCurrentLoopName(), newName);
        if (speedRatio != 1.0)
        {
            Loop loop = slowedLoops.remove(player.getCurrentLoopName());
            loop.setName(newName);
            slowedLoops.put(newName, loop);
        }
        hasUnsavedChanges = true;
    }

    public String getCurrentLoopName()
    {
        return player.getCurrentLoopName();
    }

    public void deleteLoop(String loopName)
    {
        currentSaveObject.removeLoop(loopName);
        slowedLoops.remove(loopName);
        hasUnsavedChanges = true;
    }

    public void addTrackEndListener(TrackEndListener listener)
    {
        player.addTrackEndListener(listener);
    }

    public void removeTrackEndListener(TrackEndListener listener)
    {
        player.removeTrackEndListener(listener);
    }

    public File getCurrentSaveFile()
    {
        return currentSaveObject.getSaveFile();
    }

    public File getCurrentAudioFile()
    {
        return currentSaveObject.getAudioFile();
    }

    
// SaveState interface implementations
    @Override
    public boolean hasUnsavedChanges()
    {
        return hasUnsavedChanges;
    }


    @Override
    public void save(File file) throws SaveStateOperationFailedException
    {
        File previousSaveFile = currentSaveObject.getSaveFile();
        try (FileOutputStream fileOutputStream = new FileOutputStream(file);
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream))
        {
            
            currentSaveObject.setSaveFile(file);
            objectOutputStream.writeObject(currentSaveObject);
            hasUnsavedChanges = false;
        }
        catch (IOException ex)
        {
            currentSaveObject.setSaveFile(previousSaveFile);
            throw new SaveStateOperationFailedException("Failed to save");
        }
        
    }

    @Override
    public boolean isSavedExternally()
    {
        return currentSaveObject.getSaveFile() != null && currentSaveObject.getSaveFile().exists();
    }

    @Override
    public File getExternalFile()
    {
        return currentSaveObject.getSaveFile();
    }

    @Override
    public void open(File file) throws SaveStateOperationFailedException
    {
        try
        {
            // load savefile
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            SaveFile newSaveObject = (SaveFile) objectInputStream.readObject();
            fileInputStream.close();
            objectInputStream.close();

            // initialise looper
            inputStream = AudioSystem.getAudioInputStream(newSaveObject.getAudioFile());
            AudioFormat newAudioFormat = inputStream.getFormat();
            byte[] newBuffer = new byte[Math.toIntExact(framesToBytes(inputStream.getFrameLength(), newAudioFormat))];
            // read in all the samples from the file.
            int read = 0;
            while (inputStream.read(newBuffer, read, 1000) >= 0)
            {
                read += 1000;
            }
            
            Logger.getLogger(Model.class.getName()).log(Level.FINE, "New audio file read to buffer OK: {0} bytes.", read);

            SourceDataLine newSourceDataLine = AudioSystem.getSourceDataLine(newAudioFormat, AudioSystem.getMixerInfo()[1]);
            Logger.getLogger(Model.class.getName()).log(Level.FINE, "New source dataline created OK");
            newSourceDataLine.open(newAudioFormat, Math.toIntExact(newAudioFormat.getFrameSize() * 10000));

            Logger.getLogger(Model.class.getName()).log(Level.FINE, "Source dataline opened OK");
            // All possible exceptions hae been passed so safe to update the in-memory savestate.
            currentSaveObject = newSaveObject;
            buffer = newBuffer;
            audioFormat = newAudioFormat;
            sourceDataLine = newSourceDataLine;
            player.initialise(buffer, audioFormat, Math.toIntExact(inputStream.getFrameLength()), sourceDataLine, (Loop) currentSaveObject.getLoops().values().toArray()[0]);
            inputStream.close();
            inputStream = null;

        }
        catch (IOException | ClassNotFoundException | UnsupportedAudioFileException | LineUnavailableException ex)
        {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, "Failed to open existing save file: " + file.getPath(), ex);
            throw new SaveStateOperationFailedException("Load operation failed");
        }
        hasUnsavedChanges = false;
    }

    /**
     * Sets the program to its default state. Loads a new blank savefile.
     * <p>
     */
    @Override
    public void newSaveState()
    {
        this.currentSaveObject = new SaveFile();
        this.speedRatio = 1.0;
        this.buffer = null;
        this.slowedAudio = null;
        this.slowedLoops.clear();
        player.reset();
        if (inputStream != null)
        {
            try
            {
                inputStream.close();
            }
            catch (IOException ex)
            {
                Logger.getLogger(Model.class.getName()).log(Level.SEVERE, "Failed to close input stream while resetting save state", ex);
                System.exit(0);
            }
        }
        if (sourceDataLine != null)
        {
            sourceDataLine.close();
        }
        audioFormat = null;
    }

    public float getSampleRate()
    {
        return audioFormat.getSampleRate();
    }
}
