/*
 * The MIT License
 *
 * Copyright 2017 John Marshall.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uk.co.jmsoft.transcriptiontool.core;

import java.io.Serializable;

/**
 *
 * @author Scooby
 */
public class Loop implements Serializable
{
    private String name = "";
    private int startFrameIndex = 0;
    private int endFrameIndex = 0;

    
    protected Loop(String name)
    {
        this.name = name;
    }

    
    protected String getName()
    {
        return name;
    }

    protected void setName(String name)
    {
        this.name = name;
    }

    protected int getEndFrameIndex()
    {
        return endFrameIndex;
    }

    protected int getStartFrameIndex()
    {
        return startFrameIndex;
    }

    protected void setEndFrameIndex(int endFrame)
    {
        this.endFrameIndex = endFrame;
    }

    protected void setStartFrameIndex(int startFrame)
    {
        this.startFrameIndex = startFrame;
    }

/**
 * Creates a duplicate of the loop object with the new name.
     * @param name
 * @return 
 */
    protected Loop copy(String name)
    {
        Loop result = new Loop(name);
        result.setStartFrameIndex(startFrameIndex);
        result.setEndFrameIndex(endFrameIndex);
        return result;
    }
    
    
}
