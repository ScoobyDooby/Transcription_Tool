/*
 * The MIT License
 *
 * Copyright 2017 John Marshall.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uk.co.jmsoft.transcriptiontool.core;

import uk.co.jmsoft.transcriptiontool.exceptions.PlayModeException;
import uk.co.jmsoft.transcriptiontool.exceptions.BufferBoundsException;
import uk.co.jmsoft.transcriptiontool.exceptions.LooperNotInitialisedException;
import uk.co.jmsoft.transcriptiontool.exceptions.LoopBoundsException;
import java.util.HashSet;
import java.util.Set;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.SourceDataLine;

/**
 * Provides playing, looping and slowdown of preloaded audio files.
 * <p>
 * @author Scooby
 */
public class Player
{

    protected boolean isPlaying()
    {
        return playing;
    }

    public enum PlayMode
    {

        PLAY,
        LOOPEDPLAY,
        NOT_PLAYING

    }

    private SourceDataLine sourceDataLine;
    private AudioFormat format;
    private final Set<TrackEndListener> trackEndListeners = new HashSet<>();

    /**
     * The buffered audio.
     */
    private byte[] buffer;
 
    /*
     The current frame position in the whole track.
     */
    private int currentFrame = 0;
    private Loop loop = new Loop("Default");
    private int totalFrames = 0;
    private boolean initialised = false;
    private boolean playing = false;
    private boolean looping = false;
    private double speedRatio = 1.0;

    /**
     * Assigns a new buffered audio file to the looper. This resets the looper
     * and automatically stops the currently playing file.
     * <p>
     * @param buffer
     * @param format
     * @param totalFrames
     * @param sourceDataLine
     * @param loop           - If null a loop will be set to loop over the
     *                       entire track.
     * <p>
     */
    protected synchronized void initialise(byte[] buffer, AudioFormat format,
            int totalFrames, SourceDataLine sourceDataLine, Loop loop)
    {
        playing = false;
        looping = false;
        initialised = false;
        // TODO Looper Wait for play Thread to finish before reinitialising.

        this.format = format;
        this.totalFrames = totalFrames;
        setBuffer(buffer);
        this.sourceDataLine = sourceDataLine;
        // Initialise the loop to the entire track if null.
        if (loop == null)
        {
            this.loop = new Loop("Default");
            this.loop.setStartFrameIndex(0);
            this.loop.setEndFrameIndex(totalFrames);
        }
        else
        {
            this.loop = loop;
        }
        initialised = true;
    }



    /**
     * Sets the sample to end the loop on during looped play. Must be after
     * the sample currently assigned as the start of the loop, otherwise throws
     * an exception.
     * <p>
     * @param newEnd
     *               <p>
     * @throws LoopBoundsException
     */
    protected synchronized void setEndFrameIndex(int newEnd) throws LoopBoundsException
    {
      

    }

    /**
     * Returns whether the looper is ready to play. The looper must be
     * initialised by a call to initialise to become ready.
     * <p>
     * @return
     */
    protected synchronized boolean isInitialised()
    {
        return initialised;
    }

    /**
     * Stops play.
     */
    protected void stop()
    {
        playing = false;
    }

    /**
     * Starts playing in looped play mode. Play can only happen in one mode at a
     * time. If already playing looped, play continues.
     * Looper must be initialised by calling initialise() before it can play.
     * <p>
     * @throws LooperNotInitialisedException
     */
    protected void playLooped() throws LooperNotInitialisedException
    {
        if (!initialised)
        {
            throw new LooperNotInitialisedException();
        }
        if (!playing)
        {
            playing = true;
            looping = true;
            Thread thread = new Thread(loopPlayRunnable, "looperThread");
            thread.start();
        }
    }

    /**
     * Starts playing in non-looped mode. All samples in the buffer starting at
     * the current transport position will be played once. Play can only happen
     * in one mode at a time.
     * <p>
     * @throws LooperNotInitialisedException
     */
    protected void play() throws LooperNotInitialisedException
    {
        if (!initialised)
        {
            throw new LooperNotInitialisedException();
        }
        if (!playing)
        {
            playing = true;
            Thread thread = new Thread(playRunnable, "playThread");
            thread.start();
        }
    }

    private void limitLocalIndexToLoopBounds()
    {
        if (currentFrame < loop.getStartFrameIndex() || currentFrame >= loop.getEndFrameIndex())
        {
            currentFrame = loop.getStartFrameIndex();
        }
    }

    private final Runnable loopPlayRunnable = new Runnable()
    {

        @Override
        public void run()
        {

            sourceDataLine.flush();
            sourceDataLine.start();
            int frameSize = format.getFrameSize();
            limitLocalIndexToLoopBounds();

            while (playing)
            {
                // Spin loop until source dataline input buffer available
                // This should keep the sample time in synch with 
                // the clock time.
                int availableBytes = sourceDataLine.available();
                if (availableBytes > frameSize)
                {
                    int availableFrames = Math.toIntExact(Model.bytesToFrames(availableBytes, format));
                    writeToSourceInLoopedPlayMode(availableFrames);
                }
            }

            sourceDataLine.stop();
            looping = false;
        }

    };

    private final Runnable playRunnable = new Runnable()
    {

        @Override
        public void run()
        {
            sourceDataLine.flush();
            sourceDataLine.start();

            int frameSize = format.getFrameSize();

            // Stop playing once end of buffer is reached.
            while (playing && totalFrames > currentFrame)
            {
                int availableBytes = sourceDataLine.available();
                if (availableBytes > frameSize)
                {
                    int availableFrames = Math.toIntExact(Model.bytesToFrames(availableBytes, format));
                    writeToSourceInPlayMode(availableFrames);
                }
            }

            // Make sure the whole track is played before stopping.
            sourceDataLine.drain();
            sourceDataLine.stop();
            playing = false;
            notifyTrackEndListeners();
        }

    };

    /**
     * The write method used when in looping playback. Handles limiting the
     * input within the upper and lower bounds.
     * <p>
     * @param frames
     */
    private synchronized void writeToSourceInLoopedPlayMode(int frames)
    {
        int framesRemaining = Math.min(frames, loop.getEndFrameIndex() - currentFrame);

        while (framesRemaining > 0)
        {
            // Try and write the next lot of bytes.
            int writtenBytes = sourceDataLine.write(buffer, Math.toIntExact((Model.framesToBytes(currentFrame, format))), Math.toIntExact(Model.framesToBytes(framesRemaining, format)));
            int writtenFrames = Math.toIntExact(Model.bytesToFrames(writtenBytes, format));
            framesRemaining -= writtenFrames;
            currentFrame += writtenFrames;
        }
        if (currentFrame == loop.getEndFrameIndex())
        {
            currentFrame = loop.getStartFrameIndex();
        }
    }

    /**
     * The write method used when in non-looping playback. Handles limiting the
     * input within the upper and lower bounds.
     * <p>
     */
    private synchronized void writeToSourceInPlayMode(int availableFrames)
    {
        int framesToEnd = totalFrames - currentFrame;
        int framesRemaining = Math.min(framesToEnd, availableFrames);

        while (framesRemaining > 0)
        {
            int bytesWritten = sourceDataLine.write(buffer, Math.toIntExact(Model.framesToBytes(currentFrame, format)), Math.toIntExact(Model.framesToBytes(framesRemaining, format)));
            int writtenFrames = Math.toIntExact(Model.bytesToFrames(bytesWritten, format));
            framesRemaining -= writtenFrames;
            currentFrame += writtenFrames;
        }
    }

    /**
     * Moves the transport to the start of the track.
     * Should only be called in non-looping playmode or when not playing
     * otherwise throws an exception.
     * <p>
     * @throws core.PlayModeException
     */
    protected synchronized void sendTransportToTrackStart() throws PlayModeException
    {
        if (!looping)
        {
            currentFrame = 0;
        }
        else
        {
            throw new PlayModeException("restart track", PlayMode.LOOPEDPLAY);
        }
    }

    /**
     * Moves transport to start of the loop.
     * Should only be called in looping playmode or when not playing otherwise
     * an exception is thrown.
     * <p>
     * @throws uk.co.jmsoft.transcriptiontool.exceptions.PlayModeException
     */
    protected synchronized void sendTransportToLoopStart() throws PlayModeException
    {
        if (playing && !looping)
        {
            throw new PlayModeException("restart loop", PlayMode.PLAY);
        }
        else
        {
            currentFrame = loop.getStartFrameIndex();
        }
    }

    protected synchronized void setBuffer(byte[] buffer)
    {
        this.buffer = buffer;
        this.loop.setStartFrameIndex(0);
        this.loop.setEndFrameIndex(Math.toIntExact(Model.bytesToFrames(buffer.length, format)));
    }

    /**
     * Returns the index of the current sample within the loop section.
     * <p>
     * @return
     */
    protected synchronized int getCurrentFrameIndex()
    {
        return currentFrame;
    }

    protected synchronized byte[] getBuffer()
    {
        return buffer;
    }

    public int getStartFrameIndex()
    {
        return loop.getStartFrameIndex();
    }

    public int getEndFrameIndex()
    {
        return loop.getEndFrameIndex();
    }

    protected int getTotalFrames()
    {
        return totalFrames;
    }

    /**
     * Sets the current Frame relative to the whole buffer.
     * If looping the position must be within the loop bounds(startFrame to
     * EndFrame), otherwise the position must be within the buffer bounds(0 to
     * totalFrames).
     * <br>
     * <p>
     * @param samplePosition
     *                       <p>
     * @throws LoopBoundsException
     * @throws BufferBoundsException
     */
    protected synchronized void setCurrentFrameIndex(int samplePosition) throws LoopBoundsException, BufferBoundsException
    {
        if (!looping)
        {
            // Not looping so the current index can be set anywhere within buffer bounds.
            if (!(samplePosition >= 0 && samplePosition < buffer.length))
            {
                throw new BufferBoundsException();
            }
            else
            {
                currentFrame = samplePosition;
            }
        }
        else
        {
            // looping so current index must by within loop bounds.
            if (!(samplePosition >= loop.getStartFrameIndex() && samplePosition <= loop.getEndFrameIndex()))
            {
                throw new LoopBoundsException();
            }
            else
            {
                currentFrame = samplePosition;
            }
        }

    }

    /**
     * Loads a new loop. Must not be in looped play mode otherwise exception is
     * thrown.
     * <p>
     * @param loop
     *             <p>
     * @throws uk.co.jmsoft.transcriptiontool.exceptions.PlayModeException
     */
    public synchronized void setLoop(Loop loop) throws PlayModeException
    {
        if (looping)
        {
            throw new PlayModeException("Cannot set loop", PlayMode.LOOPEDPLAY);
        }
        else
        {
            this.loop = loop;
        }
    }

    /**
     * Resets the looper to its default uninitialised state.
     * Stops playing.
     * Sets the buffer to null.
     */
    protected synchronized void reset()
    {

        initialised = false;
        playing = false;
        looping = false;
        format = null;
        sourceDataLine = null;
        currentFrame = 0;
        totalFrames = 0;
        loop = new Loop("Default");
    }

    String getCurrentLoopName()
    {
        return loop.getName();
    }

    protected Loop getCurrentLoop()
    {
        return loop;
    }

    protected void addTrackEndListener(TrackEndListener listener)
    {
        this.trackEndListeners.add(listener);
    }

    protected void removeTrackEndListener(TrackEndListener listener)
    {
        this.trackEndListeners.remove(listener);
    }

    private void notifyTrackEndListeners()
    {
        for (TrackEndListener eachListener : trackEndListeners)
        {
            eachListener.onTrackEnd();
        }
    }
}
