/*
 * The MIT License
 *
 * Copyright 2017 John Marshall.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uk.co.jmsoft.transcriptiontool.core;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Saves a set of loops and the path to the audio file they are
 * associated with.
 * <p>
 * @author Scooby
 */
public class SaveFile implements Serializable
{

    private File saveFile = null;
    private File audioFile = null;
    private final Map<String, Loop> loops = new HashMap<>();

    protected Map<String, Loop> getLoops()
    {
        return loops;
    }

    protected void removeLoop(String name)
    {
        loops.remove(name);
    }

    /**
     * Creates a new loop with the given name and adds it to the list of loops.
     * <p>
     * @param name
     */
    protected void addNewLoop(String name)
    {
        loops.put(name, new Loop(name));
    }

    protected File getAudioFile()
    {
        return audioFile;
    }

    protected void setAudioFile(File audioFile)
    {
        this.audioFile = audioFile;
    }

    protected Loop getLoop(String name)
    {
        return loops.get(name);
    }

    protected void addLoop(Loop newLoop)
    {
        loops.put(newLoop.getName(), newLoop);
    }

    protected void renameLoop(String currentLoopName, String newName)
    {
        Loop currentLoop = getLoop(currentLoopName);
        currentLoop.setName(newName);
        loops.remove(currentLoopName);
        loops.put(newName, currentLoop);
    }

    /**
     * Returns the File object representing the file on disk or null if there is
     * no such object.
     * <p>
     * @return
     */
    public File getSaveFile()
    {
        return saveFile;
    }

    public void setSaveFile(File saveFile)
    {
        this.saveFile = saveFile;
    }

}
