/**
 * <h1>Transcription Tool</h1>
 * <p>
 * A Java GUI application for looping and slowing down sections of audio to aid in transcribing music.
 * </p>
 * <h2>Features</h2>
 * <ul>
 * <li>Graphical user interface</li>
 * <li>Loop through sections of an audio track.</li>
 * <li>Slow down the audio without changing the pitch to help make fast passages of notes easier to transcribe.</li>
 * <li>Save multiple loops and give them names.</li>
 * <li>Save all your loops for a given track to file.</li>
 * </ul>
 */
package uk.co.jmsoft.transcriptiontool;
